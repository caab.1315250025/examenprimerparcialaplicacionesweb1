const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');

const expresiones = {
    codigo: /^[a-zA-Z0-9]{1,5}$/, // Letras, numeros.
    Marca: /^[a-zA-Z0-9]{1,50}$/, // Letras, numeros.
    modelo: /^[a-zA-Z0-9]{1,30}$/, // Letras, numeros.
    año: /^\d{1,4}$/, // 1 a 4 numeros.
}

const validarFormulario = (e) => {
    switch (e.target.name) {
        case "codigo":
            if (expresiones.codigo.test(e.target.value)) {
                document.getElementById("mensaje1-error").innerHTML = "";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje1-error").innerHTML = "Codigo no válido";
            }
            break;
        case "Marca":

            if (expresiones.Marca.test(e.target.value)) {
                document.getElementById("mensaje2-error").innerHTML = "";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje2-error").innerHTML = "Marca no válido";
            }
            break;
        case "modelo":
            if (expresiones.modelo.test(e.target.value)) {
                document.getElementById("mensaje3-error").innerHTML = "";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje3-error").innerHTML = "Modelo no válido";
            }
            break;
        case "año":
            if (expresiones.año.test(e.target.value)) {
                document.getElementById("mensaje4-error").innerHTML = "";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje4-error").innerHTML = "Año no válido";
            }
            break;
        case "fechaInicial":
            fechaInicial = document.getElementById('fechaInicial').value;
            
            if (fechaInicial == "") {
                document.getElementById("mensaje5-error").innerHTML = "Fecha Inical no válida";
            }else{
                document.getElementById("mensaje5-error").innerHTML = "";
            }
            break;
        case "fechaFinal":
            fechaInicial = document.getElementById('fechaInicial').value;
            fechaFinal = document.getElementById('fechaFinal').value;
            
            if (fechaFinal == "") {
                document.getElementById("mensaje6-error").innerHTML = "Fecha Final no válida";
            }else{
                document.getElementById("mensaje6-error").innerHTML = "";
            }

            if (fechaInicial > fechaFinal) {
                document.getElementById("mensaje6-error").innerHTML = "La fecha Final no puede ser menor que la fecha inicial.";
            }else{
                document.getElementById("mensaje6-error").innerHTML = "";
            }
            break;
    }
}

inputs.forEach((input) => {
    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    var codigo = document.getElementById('codigo').value;
    var Marca = document.getElementById('Marca').value;
    var modelo = document.getElementById('modelo').value;
    var año = document.getElementById('año').value;
    var fechaInicial = document.getElementById('fechaInicial').value;
    var fechaFinal = document.getElementById('fechaFinal').value;
    if (!codigo.trim("") || !Marca.trim("") || !modelo.trim("") || !año.trim("")
        || !fechaInicial.trim("") || !fechaFinal.trim("")) {
        document.getElementById("mensaje-error").innerHTML = "Ingrese todos los campos";
    } else {
        location.reload();
        document.form1.submit();
    }
});